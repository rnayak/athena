#include "InDetTrackScoringTools/InDetAmbiScoringTool.h"
#include "InDetTrackScoringTools/InDetCosmicScoringTool.h"
#include "InDetTrackScoringTools/InDetTrtTrackScoringTool.h"
#include "../ROIInfoVecAlg.h"

DECLARE_COMPONENT( InDet::InDetAmbiScoringTool )
DECLARE_COMPONENT( InDet::InDetCosmicScoringTool )
DECLARE_COMPONENT( InDet::InDetTrtTrackScoringTool )
DECLARE_COMPONENT( ROIInfoVecAlg )

